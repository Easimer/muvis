#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *
from pulseaudio_monitor import PeakMonitor
import json
import sys
import subprocess
import math
import time
import shlex

screen = None
size = None
conf = None
exit = False
bg_color = (255, 255, 255)
fg_color = (0, 0, 0)
speed_fall = 10
speed_raise = 150

font_title = None
font_album = None
font_color = (51, 51, 51)

rhythmbox_pid = False
rb_surface = None


last_player_check = 0.0
last_player_data = None

last_config_check = 0.0
config_txt = ""

tower_n = 128

#Betölti a konfigurációs fájlt, beállít értékeket, majd visszatér a konfigurációval
def load_config(fn = "config.json"):
	global size
	global bg_color
	global fg_color
	global font_title
	global font_album
	global font_color
	global towers
	global config_txt
	global rhythmbox_pid
	global speed_raise
	global speed_fall
	conf = None
	#open json file
	with open(fn, "r") as cfile:
		config_txt = cfile.read()
		conf = json.loads(config_txt)
	#set stuff
	size = conf["graphics"]["resolution"]
	id_color = conf["graphics"]["color"]
	found = False
	for color in conf["graphics"]["colors"]: #find color by id
		if color["id"] == id_color:
			bg_color = color["bg"]
			fg_color = color["fg"]
			if color["fg_font"]:
				font_color = color["fg"]
			else:
				try:
					font_color = color["font"]
				except KeyError:
					pass
			print("Using theme %s" % id_color)
			try:
				author = color["author"]
				print("Made by: %s" % author)
			except KeyError:
				pass
			found = True
		elif color["id"] == "default" and not found: #if the id is default and we haven't found the theme set in config
			bg_color = color["bg"]
			fg_color = color["fg"]
			font_color = (color["font"])
	font_title = pygame.font.Font(conf["graphics"]["font"]["family"], conf["graphics"]["font"]["title_size"])
	font_album = pygame.font.Font(conf["graphics"]["font"]["family"], conf["graphics"]["font"]["album_size"])
	mode_flags = 0
	if conf["graphics"]["fullscreen"]:
		mode_flags = mode_flags | pygame.FULLSCREEN
	screen = pygame.display.set_mode(size, mode_flags)
	if conf["settings"]["rb_enabled"]:
		rhythmbox_pid = get_process_pid("rhythmbox")
	else:
		rhythmbox_pid = False
	speed_raise = conf["settings"]["speed_raise"]
	speed_fall = conf["settings"]["speed_fall"]
	tower_n = conf["settings"]["towers"]
	return conf


#Lekéri a folyamat számát
def get_process_pid(process):
	try:
		return int(subprocess.check_output(["pidof", process]).replace("\n", ""))
	except:
		return False

def get_rhythmbox_player():
	global last_player_data
	global last_player_check
	if time.time() - last_player_check >= 1 or last_player_data == None:
		last_player_check = time.time()
		data = list()
		formats = ["%ta", "%tt", "%at", "%te"]
		for f in formats:
			proc = subprocess.Popen(shlex.split('rhythmbox-client --no-start --print-playing-format "%s"' % f), stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
			d,e = proc.communicate()
			data.append(d.replace("\n", ""))
		last_player_data = data
		return data
	else:
		return False

if __name__ == '__main__':
	pygame.init()
	load_config()
	screen = pygame.display.set_mode(size)
	pygame.display.set_caption("Music Visualizer")
	screen.fill(bg_color)
	towers = list()
	if not rb_surface:
		rb_surface = pygame.Surface(size, flags = SRCALPHA)
	for i in xrange(tower_n):
		towers.append(0)
	#main loop
	for sample in PeakMonitor("alsa_output.pci-0000_00_1b.0.analog-stereo", tower_n, "ayylmao"):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()
			if event.type == pygame.KEYDOWN:
					if event.key == K_x:
						sys.exit()
		screen.fill(bg_color)
		towers[sample - 1] += speed_raise if towers[sample - 1] < size[1] - size[1] / 4 else 0
		towers[tower_n - 1] = 0
		for i in xrange(tower_n):
			towers[i - 1] -= speed_fall if towers[i - 1] > 0 else 0
			pygame.draw.rect(screen, fg_color, (i * (size[0] / tower_n), size[1], size[0] / tower_n, towers[i] * -1))
		if rhythmbox_pid:
			player = get_rhythmbox_player()
			if player:
				surface_title = font_title.render(player[0] + " - " + player[1], True, font_color)
				surface_album = font_album.render(player[2], True, font_color)
				surface_time = font_album.render(player[3], True, font_color)
				rb_surface.fill((255, 255, 255, 0))
				rb_surface.blit(surface_title, Rect(10, 10, surface_title.get_width(), surface_title.get_height()))
				rb_surface.blit(surface_album, Rect(10, 10 + surface_title.get_height(), surface_album.get_width(), surface_album.get_height()))
				rb_surface.blit(surface_time, Rect(10, 10 + surface_title.get_height() + surface_album.get_height(), surface_album.get_width(), surface_album.get_height()))
			screen.blit(rb_surface, Rect(0, 0, rb_surface.get_width(), rb_surface.get_height()))
		pygame.display.flip()